#### Basic WordPress branding

Mit diesem Plugin werden Grundlagen für eine neue WordPress Installation festgelegt bzw. geändert.

Status: alpha

Hier eine Liste der Sachen die für einen optimalen Einsatz benötigt werden:

 > Login Logo (min.85x85px,png)
 > Dashboard Logo (20x20px,png)
 > Inhalt vom Dashboard Widget (optional)

ToDo:
 - Sinnvolle funktionen/erweiterungen?
 - Dokumentation

current functions:
  - Versionsinfo abschalten
  - Head Bereich aufräumen
  - Login Fehler begrenzen
  - Login Logo setzen
  - Login Logo URL setzen
  - Login Logolink titel setzen
  - seitensprung bei -read more- abschalten
  - Shortcodes in Text-Widgets
  - RSS-Feeds abschalten
  - Dashboard Widgets abschalten
  - Eigenes Dashboard Widget anlegen
  - Dashboard Logo setzen
  - Admin Footer Text setzen
  - Dashicons aktivieren

Anleitung und Dokumentation wird noch ausgearbeitet.

Benutzung auf eigene Gefahr, sollte aber eigentlich auch nix kaputt machen...eigentlich..

