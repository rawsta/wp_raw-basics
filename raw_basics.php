<?php
/**
 *
 * Plugin Name: Rawsta Basics
 * Plugin URI: http://www.instantostrich.com
 * Description: Grundeinstellungen für eine neue Webseite
 * Version: 0.8
 *
 * Author: Sebastian Fiele
 * Author URI: http://www.rawsta.de
 * 
 * Text Domain: raw_bsx
 *
 * current functions:
 * - Versionsinfo abschalten
 * - Login Fehler begrenzen
 * - Login Logo setzen
 * - Login Logo URL setzen
 * - Login Logolink titel setzen
 * - seitensprung bei -read more- abschalten
 * - Shortcodes in Text-Widgets 
 * - RSS-Feeds abschalten
 * - Dashboard Widgets abschalten
 * - Eigenes Dashboard Widget anlegen
 * - Dashboard Logo setzen
 * - Admin Footer Text setzen
 * - Dashicons aktivieren
 *
 */

// WP version info aus head und feeds entfernen
function rawsta_no_version() {
    return '';
}
add_filter('the_generator', 'rawsta_no_version');

// Head Bereich aufräumen
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Login Fehlerausgabe begrenzen
function rawsta_no_login_error(){
  return 'Da ist etwas schiefgelaufen!';
}
add_filter( 'login_errors', 'rawsta_no_login_error' );


// ---------- Login Page ----------- //  


// Login Logo -logo.jpg
function rawsta_login_logo() {
    echo '<style type="text/css"> h1 a { background-image:url('. plugins_url( 'img/login-logo.png',__FILE__) .') !important; }</style>';
   }
   add_action('login_head', 'rawsta_login_logo');
 
// Login Logo URL   
function rawsta_login_url($login_header_url) {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'rawsta_login_url' );
 
// Login Logo Link-title 
function rawsta_login_title($login_header_title) {
    return get_bloginfo('title');
}
add_filter( 'login_headertitle', 'rawsta_login_title' );


// ---------- Front End ---------- //   


// Shortcodes in Text-Widgets aktivieren
add_filter('widget_text','do_shortcode');

// Seitensprung bei Klick auf "Weiterlesen" entfernen
function rawsta_no_jump_link($link) {
    $offset = strpos($link, '#more-');
    if ($offset) {
        $end = strpos($link, '"',$offset);
    }
    if ($end) {
        $link = substr_replace($link, '', $offset, $end-$offset);
    }
    return $link;
}
add_filter('the_content_more_link', 'rawsta_no_jump_link');

// Die totale Abschaltung der RSS-Feeds
function rawsta_no_feed() {
wp_die( __('Kein Feed verfügbar. Bitte besuchen Sie unsere <a href="'. get_bloginfo('url') .'">Startseite</a>!') );
}

add_action('do_feed', 'rawsta_no_feed', 1);
add_action('do_feed_rdf', 'rawsta_no_feed', 1);
add_action('do_feed_rss', 'rawsta_no_feed', 1);
add_action('do_feed_rss2', 'rawsta_no_feed', 1);
add_action('do_feed_atom', 'rawsta_no_feed', 1);
 

// ---------- Dashboard ---------- //


// Abschalten der Dashboard-Widgets
function rawsta_no_dash_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
} 
add_action('wp_dashboard_setup', 'rawsta_no_widgets' );
 
// Ein eigenes Dashboard-Widget erstellen
function rawsta_dash_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Weil ich das kann.', 'rawsta_dash_help');
    }
function rawsta_dash_help() {
    echo '<p>Willkommen auf Ihrer neuen Webseite! <br />' . 
        'Die Seite schafft <?php echo get_num_queries(); ?> queries in nur <?php timer_stop(1); ?> Sekunden.<br />' . 'Bei Fragen kontaktieren Sie uns <a href="mailto:rawsta@rawsta.de">hier</a>. Für mehr Support : <a href="http://www.rawsta.de/support" target="_blank">Rawsta Support</a></p>';
    }
add_action('wp_dashboard_setup', 'rawsta_dash_widgets');

// Eigenes Logo für das Dashboard
function tri_dash_logo() {
    echo '<style type="text/css">
    #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
        background-image: url('. plugins_url( 'img/dash-logo.png',__FILE__) .') !important;
        background-position: 0 0;
        color:rgba(0, 0, 0, 0);
    }
    #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
    background-position: 0 0;
    }</style>';
}
add_action('wp_before_admin_bar_render', 'tri_dash_logo');

// Dashboard Footer Text anpassen
function rawsta_footer_admin () {
    echo 'Gestaltet mit Herzblut von  <a href="http://www.rawsta.de" target="_blank">Rawsta</a></p>';
}
add_filter('admin_footer_text', 'rawsta_footer_admin');


// enqueue Dashicons
function rawsta_enqueue_dashicons() {
     wp_enqueue_style( 'rawsta-dashicons-font', get_stylesheet_directory_uri(), array('dashicons'), '1.0' );
}
add_action( 'wp_enqueue_scripts', 'rawsta_enqueue_dashicons' );

?>
